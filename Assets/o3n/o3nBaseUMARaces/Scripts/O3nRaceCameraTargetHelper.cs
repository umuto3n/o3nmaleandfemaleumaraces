﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class O3nRaceCameraTargetHelper : MonoBehaviour {

    public UMA.Examples.MouseOrbitImproved Orbitor;
    public UMA.CharacterSystem.Examples.TestCustomizerDD testCustomizer;

    string lastKnownRace = null;

    public void LateUpdate()
    {
        if (testCustomizer!= null && testCustomizer.Avatar != null && testCustomizer.Avatar.activeRace != null && lastKnownRace != testCustomizer.Avatar.activeRace.name)
        {
            lastKnownRace = testCustomizer.Avatar.activeRace.name;
            testCustomizer.TargetBody();
            TargetBody();
        }
    }

    /// <summary>
	/// Point the mouse orbitor at the body center
	/// </summary>
	public void TargetBody()
    {
        if (Orbitor != null)
        {
            Orbitor.distance = 2.4f;
            if (testCustomizer.Avatar.activeRace.name.Contains("o3n"))
            {
                TargetBodyO3n();
            }
        }
    }

    /// <summary>
    /// Point the mouse orbitor at the neck, so you can see the face.
    /// </summary>
    public void TargetFace()
    {
        if (Orbitor != null)
        {
            Orbitor.distance = 0.5f;
            if (testCustomizer.Avatar.activeRace.name.Contains("o3n"))
            {
                TargetFaceO3n();
            }
        }
    }

    /// <summary>
    /// Point the mouse orbitor at the body center
    /// </summary>
    private void TargetBodyO3n()
    {
        if (Orbitor != null)
        {
            Orbitor.TargetBone = "Root/Global/Position/pelvis/spine01";
        }
    }

    /// <summary>
    /// Point the mouse orbitor at the neck, so you can see the face.
    /// </summary>
    private void TargetFaceO3n()
    {
        if (Orbitor != null)
        {
            Orbitor.TargetBone = "Root/Global/Position/pelvis/spine01/spine02/spine03/neck/head";
        }
    }
}
