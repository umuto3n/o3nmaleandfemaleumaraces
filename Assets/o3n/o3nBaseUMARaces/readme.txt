o3n Male and Female UMA Races
-----------------------------

This package contains custom male and female UMA races created from Manuel Bastioni models. 
UMA is required for this package to work. Special thanks to UMA Steering group, They made this possible
by giving "the" character framework to Unity Community. And also for letting me modify two of the 
UMA hair models to include in this package.


Requirements :
--------------
- UMA 2.6 or newer is required to be able to use this package (https://www.assetstore.unity3d.com/en/#!/content/35611)
(Import renders o3n race recipes unusable due to an issue with UMA 2.5 hence the requirement for 2.6)
- See the license.txt for Manuel Bastioni models' CC-BY license terms. (for more info on CC-BY license and
 ManuelBastioni Lab http://www.manuelbastioni.com/faq.php)


Directions   :
--------------
- Please remember to add the o3n folder to UMA Global Library by selecting UMA -> Global Library Window
and dragging the o3n folder to the drop area in the window.
- You can open the ExampleScene under o3n/o3nUMARaces/ExampleScenes/ to see a sample scene with o3n male and female.
- Race folders have Prefabs folders which contains prefabs that you can drag in to your UMA ready scene. 
(See the example scene)


Relese Notes (Version 1.0)
-------------------------------
- Initial release


Relese History 
--------------
None