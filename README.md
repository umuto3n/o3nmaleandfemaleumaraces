# README #

IMPORTANT! This package is discontinued in favor of a new base races set which is not using bastioni races as base. New races are not bound by CC-BY. This package will not be updated. You can access the new package at https://bitbucket.org/umuto3n/o3numaraces2

- UMA is required to be able to use this package (https://www.assetstore.unity3d.com/en/#!/content/35611)
- See the license section for Manuel Bastioni models' CC-BY license terms.

### What is this repository for? ###

This project contains custom male and female UMA races created from Manuel Bastioni models. 
UMA is required for this package to work. Special thanks to UMA Steering group, They made this possible
by giving "the" character framework to Unity Community. And also for letting me modify two of the 
UMA hair models to include in this package.

Check the product page at http://www.o3n.org/o3n-male-and-female-uma-races/

### How do I get set up? ###

- Please remember to add the o3n folder to UMA Global Library by selecting UMA -> Global Library Window
and dragging the o3n folder to the drop area in the window.
- You can open the ExampleScene under o3n/o3nUMARaces/ExampleScenes/ to see a sample scene with o3n male and female.
- Race folders have Prefabs folders which contains prefabs that you can drag in to your UMA ready sceene (See example scene).

### Who do I talk to? ###

* You can contact us at support@o3n.org

### License ###

The o3n male and female 3d models are generated using the open source tool “ManuelbastioniLAB” 
so o3n male and female models in this package are derivative of the characters created by 
Manuel Bastioni (www.manuelbastioni.com) and are licensed under CC-BY 4.0. 

All the changes to the exported bastioni models are listed below:

- Male and female models are exported with 0.5 on every slider value. So the base models are 
 avarage human male and female.
 
- Posture of the models are altered for a better standing A pose. (Legs rotated slightly 
 to get the feet directly below body)
 
- Added several bones to the armature and changed / modified the vertex skin weights heavily to have all
 the adjustment capability of UMA framework.
 
- Added several face bones and rigged the face to have the adjustments of face available (bastioni face
 morphs have not been used)
 
- Textures are altered, namely; removed the hair to its own texture so that it can be put on and off,
 Changed color of skin so it is lighter and can be colored at will at runtime.
 
- Modified models' fbx files are included in the package.

Please check out the bastioni faq page for the requirements of the CC-BY license 
(http://www.manuelbastioni.com/faq.php)
